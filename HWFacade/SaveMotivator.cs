﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;

namespace HWFacade
{
    public static class SaveMotivator
    {
        public static void Save(Image image, String nameMotivator, ImageFormat format)
        {
            image.Save($"D:\\{nameMotivator}.jpg", ImageFormat.Jpeg);
        }

    }
}
