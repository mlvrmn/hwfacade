﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace HWFacade
{
    public class LoaderImage
    {
        public Image Load(String pathImage)
        {
            Image image = Bitmap.FromFile(pathImage);

            return image;
        }
    }
}
