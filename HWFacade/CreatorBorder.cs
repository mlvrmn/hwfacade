﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace HWFacade
{
    public class CreatorBorder
    {
        public void Create(Image image)
        {
            Graphics graphics = Graphics.FromImage(image);
            graphics.DrawRectangle(new Pen(Color.HotPink, 100), 0, 0, image.Width, image.Height);
        }
    }
}

