﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace HWFacade
{
    public class CreatorText
    {
        public void Create(Image image, String message)
        {
            Graphics graphics = Graphics.FromImage(image);

            graphics.DrawString(message, new Font("Verdana", (float)15), new SolidBrush(Color.Red), image.Width / 3, image.Height / 2 + image.Height / 3);
        }
    }
}
