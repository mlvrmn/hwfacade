﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;

namespace HWFacade
{
    public class CreatorMotivator
    {
        public String CreateMotivator(String imagePath, String text)
        {
            LoaderImage loaderImage = new LoaderImage();
            Image image = loaderImage.Load(imagePath);

            CreatorText creatorText = new CreatorText();
            creatorText.Create(image, text);

            Console.WriteLine("Введите название мотиватора");
            String nameMotivator = Console.ReadLine();

            CreatorBorder creatorBorder = new CreatorBorder();
            creatorBorder.Create(image);

            SaveMotivator.Save(image, nameMotivator, ImageFormat.Jpeg);

            return nameMotivator;
        }
    }
}
