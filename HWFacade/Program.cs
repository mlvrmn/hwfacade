﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace HWFacade
{

    class Program
    {

        static void Main(string[] args)
        {
            Console.WriteLine("Введите путь к картинке для создания мотиватора.");
            String pathImage = Console.ReadLine();

            Console.WriteLine("Введите текст для мотивации :)");
            String text = Console.ReadLine();

            CreatorMotivator creator = new CreatorMotivator();

            String name = creator.CreateMotivator(pathImage, text);

            Console.WriteLine($"Мотиватор {name} создан");
           
            Console.ReadKey();
        }
    }
}
